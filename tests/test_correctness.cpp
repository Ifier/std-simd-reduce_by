#include "../interface.hpp"

#include <iostream>
#include <random>
#include <limits>
#include <typeinfo>

namespace stdx = std::experimental;

static std::mt19937 s_random{ std::random_device{}() };

template<typename T>
stdx::native_simd<T> random_vector()
{
  std::uniform_int_distribution<T> distrib(std::numeric_limits<T>::min(), std::numeric_limits<T>::max());
  stdx::native_simd<T> v;
  for (int i = 0; i != v.size(); ++i) { v[i] = distrib(s_random); }
  return v;
}
template<typename T, typename Abi>
std::ostream& operator<<(std::ostream& os, const stdx::simd<T,Abi> v)
{
  os << "{ ";
  for (int i = 0; i != v.size(); ++i)
  {
    os << +v[i] << ", ";
  }
  return os << "}";
}

template<typename T> const char* type_name();
template<> const char* type_name<unsigned char>(){ return "uchar"; };
template<> const char* type_name<unsigned short>(){ return "ushort"; };
template<> const char* type_name<unsigned int>(){ return "uint"; };
template<> const char* type_name<unsigned long>(){ return "ulong"; };
template<> const char* type_name<unsigned long long>(){ return "ullong"; };
template<> const char* type_name<signed char>(){ return "char"; };
template<> const char* type_name<signed short>(){ return "short"; };
template<> const char* type_name<signed int>(){ return "int"; };
template<> const char* type_name<signed long>(){ return "long"; };
template<> const char* type_name<signed long long>(){ return "llong"; };

template<typename T, typename U>
void test_resize()
{
    const stdx::native_simd<T> in_vector = random_vector<T>();
    const auto actual_vector = reduce_by<1>(in_vector, std::plus<U>{});
    std::remove_const_t<decltype(actual_vector)> expected_vector;
    for (int i = 0; i != in_vector.size(); ++i) { expected_vector[i] = static_cast<U>(in_vector[i]); }
    if (stdx::any_of(actual_vector != expected_vector))
    {
      std::cout << "Resize (" << type_name<T>() << " -> " << type_name<U>() << ") is not equivalent!" << std::endl;
      std::cout << "From " << in_vector << ':' << std::endl;
      std::cout << " - expected: " << expected_vector << std::endl;
      std::cout << " - got:      " << actual_vector << std::endl;
    }
}

template<typename T, typename U>
void test_pairwise_plus()
{
    const stdx::native_simd<T> in_vector = random_vector<T>();
    const auto actual_vector = reduce_by<2>(in_vector, std::plus<U>{});
    std::remove_const_t<decltype(actual_vector)> expected_vector;
    for (int i = 0; i != in_vector.size()/2; ++i) { expected_vector[i] = std::plus<U>{}( in_vector[i*2], in_vector[(i*2)+1] ); }
    if (stdx::any_of(actual_vector != expected_vector))
    {
      std::cout << "Pairwise plus (" << type_name<T>() << " -> " << type_name<U>() << ") is not equivalent!" << std::endl;
      std::cout << "From " << in_vector << ':' << std::endl;
      std::cout << " - expected: " << expected_vector << std::endl;
      std::cout << " - got:      " << actual_vector << std::endl;
    }
}
template<typename T>
void test_pairwise_min()
{
    const stdx::native_simd<T> in_vector = random_vector<T>();
    const auto actual_vector = reduce_by_min<2>(in_vector);
    std::remove_const_t<decltype(actual_vector)> expected_vector;
    for (int i = 0; i != in_vector.size()/2; ++i) { expected_vector[i] = std::min( in_vector[i*2], in_vector[(i*2)+1] ); }
    if (stdx::any_of(actual_vector != expected_vector))
    {
      std::cout << "Pairwise min (" << type_name<T>() << ") is not equivalent!" << std::endl;
      std::cout << "From " << in_vector << ':' << std::endl;
      std::cout << " - expected: " << expected_vector << std::endl;
      std::cout << " - got:      " << actual_vector << std::endl;
    }
}
template<typename T>
void test_pairwise_max()
{
    const stdx::native_simd<T> in_vector = random_vector<T>();
    const auto actual_vector = reduce_by_max<2>(in_vector);
    std::remove_const_t<decltype(actual_vector)> expected_vector;
    for (int i = 0; i != in_vector.size()/2; ++i) { expected_vector[i] = std::max( in_vector[i*2], in_vector[(i*2)+1] ); }
    if (stdx::any_of(actual_vector != expected_vector))
    {
      std::cout << "Pairwise max (" << type_name<T>() << ") is not equivalent!" << std::endl;
      std::cout << "From " << in_vector << ':' << std::endl;
      std::cout << " - expected: " << expected_vector << std::endl;
      std::cout << " - got:      " << actual_vector << std::endl;
    }
}

template<typename T>
void test_resize_set()
{
  test_resize<T, unsigned char>();
  test_resize<T, unsigned short>();
  test_resize<T, unsigned int>();
  test_resize<T, unsigned long>();
  test_resize<T, unsigned long long>();
  test_resize<T, signed char>();
  test_resize<T, signed short>();
  test_resize<T, signed int>();
  test_resize<T, signed long>();
  test_resize<T, signed long long>();
}
template<typename T>
void test_pairwise_plus_set()
{
  test_pairwise_plus<T, unsigned char>();
  test_pairwise_plus<T, unsigned short>();
  test_pairwise_plus<T, unsigned int>();
  test_pairwise_plus<T, unsigned long>();
  test_pairwise_plus<T, unsigned long long>();
  test_pairwise_plus<T, signed char>();
  test_pairwise_plus<T, signed short>();
  test_pairwise_plus<T, signed int>();
  test_pairwise_plus<T, signed long>();
  test_pairwise_plus<T, signed long long>();
}

int main(int argc, char** argv)
{
  test_resize_set<unsigned char>();
  test_resize_set<unsigned short>();
  test_resize_set<unsigned int>();
  test_resize_set<unsigned long>();
  test_resize_set<unsigned long long>();
  test_resize_set<signed char>();
  test_resize_set<signed short>();
  test_resize_set<signed int>();
  test_resize_set<signed long>();
  test_resize_set<signed long long>();
  
  test_pairwise_plus_set<unsigned char>();
  test_pairwise_plus_set<unsigned short>();
  test_pairwise_plus_set<unsigned int>();
  test_pairwise_plus_set<unsigned long>();
  test_pairwise_plus_set<unsigned long long>();
  test_pairwise_plus_set<signed char>();
  test_pairwise_plus_set<signed short>();
  test_pairwise_plus_set<signed int>();
  test_pairwise_plus_set<signed long>();
  test_pairwise_plus_set<signed long long>();
  
  test_pairwise_min<unsigned char>();
  test_pairwise_min<unsigned short>();
  test_pairwise_min<unsigned int>();
  test_pairwise_min<unsigned long>();
  test_pairwise_min<unsigned long long>();
  test_pairwise_min<signed char>();
  test_pairwise_min<signed short>();
  test_pairwise_min<signed int>();
  test_pairwise_min<signed long>();
  test_pairwise_min<signed long long>();
  
  test_pairwise_max<unsigned char>();
  test_pairwise_max<unsigned short>();
  test_pairwise_max<unsigned int>();
  test_pairwise_max<unsigned long>();
  test_pairwise_max<unsigned long long>();
  test_pairwise_max<signed char>();
  test_pairwise_max<signed short>();
  test_pairwise_max<signed int>();
  test_pairwise_max<signed long>();
  test_pairwise_max<signed long long>();

  return 0;
}
