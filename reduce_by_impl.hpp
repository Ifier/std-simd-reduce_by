#include "operations.hpp"
#include "vector_resize_helpers.hpp"

#if defined(__aarch64__)
#include "impl_neon/moves.hpp"
#include "impl_neon/plus.hpp"
#include "impl_neon/max.hpp"
#include "impl_neon/min.hpp"
#elif defined(__x86_64__)
#include "impl_x86/moves.hpp"
#include "impl_x86/plus.hpp"
#include "impl_x86/max.hpp"
#include "impl_x86/min.hpp"
#endif

#include <cstddef>
#include <algorithm>
#include <numeric>

template<class F, size_t TSize, VectorElementType Type, VectorElementType TargetType, size_t Narrows, size_t Widens, size_t Reduces>
REDUCE_BY_ALWAYS_INLINE inline size_t reduce_by_impl(native_vector_type* vectors, size_t num_vectors) noexcept
{
	constexpr size_t k_elements_per_vector = sizeof(native_vector_type) / (1 << TSize);
	constexpr size_t k_reduces_per_full_reduction = std::countr_zero(k_elements_per_vector);

	using narrow_op = Simd_narrow<TSize, Type, TSize - 1>;
	using widen_op = Simd_widen<TSize, Type, TSize + 1>;
	using reduce_op = Simd_pairwise_reduce<F, TSize, Type>;
	using convert_op = Simd_convert<TSize, Type, TargetType>;
	
	static_assert((Narrows == 0 || narrow_op::k_supported) &&
	              (Widens == 0 || widen_op::k_supported) &&
	              (Reduces == 0 || reduce_op::k_supported) &&
	              (compatable_vector_types_v<Type, TargetType> || convert_op::k_supported), "Minimum required operations not supported!");

	using full_narrow_op = Simd_narrow<TSize, Type, TSize - Narrows>;
	using full_widen_op = Simd_narrow<TSize, Type, TSize + Widens>;
	using full_reduce_op = Simd_full_reduce<F, TSize, Type>;
	using full_widen_reduce_op = Simd_full_widen_reduce<F, TSize, Type>;
	using widen_reduce_op = Simd_pairwise_widen_reduce<F, TSize, Type>;

	//allow widening reduces if the types match or it's the last widen
	constexpr bool k_allow_widening_reduces = (Type == TargetType || (Widens == 1 && compatable_vector_types_v<Type, TargetType>));

	if constexpr (Narrows > 0 && full_narrow_op::k_supported)
	{
		num_vectors = compress_vectors<(1 << Narrows)>( full_narrow_op{}, vectors, num_vectors );

		return reduce_by_impl<
			F,
			TSize - Narrows,
			Type,
			TargetType,
			0,
			Widens,
			Reduces
		>(vectors, num_vectors);
	}
	else if constexpr (Narrows > 0)
	{
		num_vectors = compress_vectors<2>( narrow_op{}, vectors, num_vectors );

		return reduce_by_impl<
			F,
			TSize - 1,
			Type,
			TargetType,
			Narrows - 1,
			Widens,
			Reduces
		>(vectors, num_vectors);
	}
	else if constexpr (full_widen_reduce_op::k_supported && k_allow_widening_reduces &&
	                   Widens > 0 && Reduces >= k_reduces_per_full_reduction &&
	                   k_reduces_per_full_reduction != 1)
	{
		num_vectors = collapse_pack_vectors( full_widen_reduce_op{}, vectors, num_vectors );

		return reduce_by_impl<
			F,
			TSize + 1,
			Type,
			TargetType,
			Narrows,
			Widens - 1,
			Reduces - k_reduces_per_full_reduction
		>(vectors, num_vectors);
	}
	else if constexpr (widen_reduce_op::k_supported && k_allow_widening_reduces &&
	                   Widens > 0 && Reduces > 0)
	{
		num_vectors = static_cast<size_t>(std::distance(vectors, std::transform( vectors, vectors + num_vectors, vectors, widen_reduce_op{} )));

		return reduce_by_impl<
			F,
			TSize + 1,
			Type,
			TargetType,
			Narrows,
			Widens - 1,
			Reduces - 1
		>(vectors, num_vectors);
	}
	else if constexpr (Widens > 0 && full_widen_op::k_supported)
	{
		num_vectors = expand_vectors<(1 << Widens)>( full_widen_op{}, vectors, num_vectors );

		return reduce_by_impl<
			F,
			TSize + Widens,
			Type,
			TargetType,
			Narrows,
			0,
			Reduces
		>(vectors, num_vectors);
	}
	else if constexpr (Widens > 0)
	{
		num_vectors = expand_vectors<2>( widen_op{}, vectors, num_vectors );

		return reduce_by_impl<
			F,
			TSize + 1,
			Type,
			TargetType,
			Narrows,
			Widens - 1,
			Reduces
		>(vectors, num_vectors);
	}
	else if constexpr (Type != TargetType)
	{
		//only bother doing a conversion if the types aren't binary-compatable (as with singed/unsigned integers)
		if constexpr (!compatable_vector_types_v<Type, TargetType>)
		{
			num_vectors = static_cast<size_t>(std::distance(vectors, std::transform( vectors, vectors + num_vectors, vectors, convert_op{} )));
		}

		return reduce_by_impl<
			F,
			TSize,
			TargetType,
			TargetType,
			Narrows,
			Widens,
			Reduces
		>(vectors, num_vectors);
	}
	else if constexpr (Reduces >= k_reduces_per_full_reduction && k_reduces_per_full_reduction != 1)
	{
		num_vectors = collapse_pack_vectors( full_reduce_op{}, vectors, num_vectors );

		return reduce_by_impl<
			F,
			TSize,
			Type,
			TargetType,
			Narrows,
			Widens,
			Reduces - k_reduces_per_full_reduction
		>(vectors, num_vectors);
	}
	else if constexpr (Reduces > 0)
	{
		num_vectors = reduce_vectors( reduce_op{}, vectors, num_vectors );

		return reduce_by_impl<
			F,
			TSize,
			Type,
			TargetType,
			Narrows,
			Widens,
			Reduces - 1
		>(vectors, num_vectors);
	}

	return num_vectors;
}
