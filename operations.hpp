#ifndef REDUCE_BY_OPERATIONS_H
#define REDUCE_BY_OPERATIONS_H

#include <cstddef>
#include <type_traits>
#include <array>
#include <limits>
#include <span>

#define REDUCE_BY_ALWAYS_INLINE [[gnu::always_inline]]

//type operation classifications
enum class VectorElementType
{
	Unsigned_integer,
	Signed_integer,
	Float,
	Unknown,
};
template<typename T>
constexpr VectorElementType get_vector_element_type()
{
	if constexpr (std::is_floating_point_v<T>)
	{
		return VectorElementType::Float;
	}
	else if constexpr (std::is_integral_v<T> && std::is_signed_v<T>)
	{
		return VectorElementType::Signed_integer;
	}
	else if constexpr (std::is_integral_v<T> && std::is_unsigned_v<T>)
	{
		return VectorElementType::Unsigned_integer;
	}
	else
	{
	        return VectorElementType::Unknown;
	}
}
template<VectorElementType Lhs, VectorElementType Rhs>
constexpr bool compatable_vector_types_v = ((Lhs == VectorElementType::Float) == (Rhs == VectorElementType::Float));

//function objects for min/max
template<typename T>
struct vector_max
{
	REDUCE_BY_ALWAYS_INLINE
	constexpr T operator()(T lhs, T rhs) noexcept
	{
		return std::max(lhs,rhs);
	}
};
template<typename T>
struct vector_min
{
	REDUCE_BY_ALWAYS_INLINE
	constexpr T operator()(T lhs, T rhs) noexcept
	{
		return std::min(lhs,rhs);
	}
};

//proxy object for types of a given size
template<size_t TSize>
using element_type_bytes = std::array<std::byte, (1 << TSize)>;

#if defined(__aarch64__)

#include <arm_neon.h>
using native_vector_type = uint8x16_t;

//helper to allow using preprocessor for generating all cases
inline uint8x16_t vreinterpretq_u8_u8(uint8x16_t v){ return v; }
inline uint8x16_t vreinterpretq_s8_s8(uint8x16_t v){ return v; }

#elif defined(__x86_64__)

#include <x86intrin.h>
using native_vector_type = __m128i;

#else

using native_vector_type = int;

#endif

//utility type
template<size_t N>
using vector_array = std::array<native_vector_type, N>;

//narrows a vector such that each element is truncated to USize and packed into a
//single output vector
template<size_t TSize, VectorElementType Type, size_t USize>
struct Simd_narrow
{
		static constexpr size_t k_input_size = TSize > USize ? TSize - USize : 0;
        static constexpr bool k_supported = false;
        native_vector_type operator()(vector_array<k_input_size>) const noexcept;
};

//widens a vector such that each element is extended to USize and split over two
//output vectors
template<size_t TSize, VectorElementType Type, size_t USize>
struct Simd_widen
{
		static constexpr size_t k_output_size = USize > TSize ? USize - TSize : 0;
        static constexpr bool k_supported = false;
        vector_array<k_output_size> operator()(native_vector_type) const noexcept;
};

//converts each element of a vector of to another type with the same width
template<size_t TSize, VectorElementType FromType, VectorElementType ToType>
struct Simd_convert
{
        static constexpr bool k_supported = false;
        native_vector_type operator()(native_vector_type) const noexcept;
};

//provides a suitable fill type for padding unevenly sized vectors. Example being
//that max would want this to be minval(T) and min would want maxval(T)
template<class F, typename U>
struct Simd_fill_type
{
        static constexpr U value = 0;
};
//while we're here, let's add those
template<typename T, typename U>
struct Simd_fill_type<vector_min<T>, U>
{
        static constexpr U value = std::numeric_limits<U>::max();
};
template<typename T, typename U>
struct Simd_fill_type<vector_max<T>, U>
{
        static constexpr U value = std::numeric_limits<U>::min();
};
template<class F, typename U>
constexpr U Simd_fill_type_v = Simd_fill_type<F,U>::value;

//reduces a vector to a single element
template<class F, size_t TSize, VectorElementType Type>
struct Simd_full_reduce
{
        static constexpr bool k_supported = false;
        element_type_bytes<TSize> operator()(native_vector_type) const noexcept;
};

//reduces a vector to a single element 1 wider than the input size
template<class F, size_t TSize, VectorElementType Type>
struct Simd_full_widen_reduce
{
        static constexpr bool k_supported = false;
        element_type_bytes<TSize + 1> operator()(native_vector_type) const noexcept;
};

//reduces two vectors to a single one by applying F over adjacent elements 
template<class F, size_t TSize, VectorElementType Type>
struct Simd_pairwise_reduce
{
        static constexpr bool k_supported = false;
        native_vector_type operator()(native_vector_type,native_vector_type) const noexcept;
};

//reduces a single vector by widening each element to twice it's size and then applying F over adjacent elements 
template<class F, size_t TSize, VectorElementType Type>
struct Simd_pairwise_widen_reduce
{
        static constexpr bool k_supported = false;
        native_vector_type operator()(native_vector_type) const noexcept;
};

#endif
