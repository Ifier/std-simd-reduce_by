#include <cstddef>
#include <iterator>

template<class F>
inline size_t reduce_vectors(F&& f, native_vector_type* const vectors, const size_t num_vectors) noexcept
{
    native_vector_type* in_current = vectors;
    native_vector_type* out_current = vectors;
    native_vector_type* in_end = vectors + num_vectors - (num_vectors % 2);
    for (; in_current != in_end; in_current += 2, ++out_current)
    {
        (*out_current) = f(in_current[0], in_current[1]);
    }
    if (num_vectors % 2 != 0)
    {
        //second doesn't actually matter here
        (*out_current++) = f(in_current[0], in_current[0]);
    }

	return static_cast<size_t>(std::distance(vectors, out_current));
}
template<size_t N, class F>
inline size_t compress_vectors(F&& f, native_vector_type* const vectors, const size_t num_vectors) noexcept
{
    native_vector_type* in_current = vectors;
    native_vector_type* out_current = vectors;
    native_vector_type* in_end = vectors + num_vectors - (num_vectors % N);
    for (; in_current != in_end; in_current += N, ++out_current)
    {
		std::array<native_vector_type, N> to_compress;
		std::copy_n(in_current, N, to_compress.begin());
        (*out_current) = f(to_compress);
    }
    if (num_vectors % N != 0)
    {
        //remainder doesn't actually matter here, so leave undefined
		std::array<native_vector_type, N> to_compress;
		std::copy_n(in_current, num_vectors % N, to_compress.begin());
        (*out_current++) = f(to_compress);
    }

	return static_cast<size_t>(std::distance(vectors, out_current));
}
template<size_t N, class F>
inline size_t expand_vectors(F&& f, native_vector_type* const vectors, const size_t num_vectors) noexcept
{
	using reverse_iter = std::reverse_iterator<native_vector_type*>;
	native_vector_type* const vectors_in_end = vectors + num_vectors;
	native_vector_type* const vectors_out_end = vectors + (num_vectors * N);
	
	reverse_iter in_current{ vectors_in_end };
	reverse_iter out_current{ vectors_out_end };
	const reverse_iter in_end{ vectors };
	for (; in_current != in_end; ++in_current, out_current += N)
	{
		const std::array<native_vector_type, N> out_vectors = f(*in_current);
		std::copy(out_vectors.crbegin(), out_vectors.crend(), out_current);
	}

	return static_cast<size_t>(std::distance(vectors, vectors_out_end));
}
template<class F>
inline size_t collapse_pack_vectors(F&& f, native_vector_type* const vectors, const size_t num_vectors) noexcept
{
	using result_type = std::invoke_result_t<F,native_vector_type>;
	using temp_array = std::array<result_type, sizeof(native_vector_type) / sizeof(result_type)>;
	using array_iter = typename temp_array::iterator;
	
	temp_array temp_data;
	native_vector_type* in_current = vectors;
	native_vector_type* out_current = vectors;
	array_iter temp_current = temp_data.begin();
	native_vector_type* const in_end = vectors + num_vectors;
	for (; in_current != in_end; ++in_current, ++temp_current)
	{
		if (temp_current == temp_data.end())
		{
			temp_current = temp_data.begin();
			(*out_current++) = std::bit_cast<native_vector_type>(temp_data);
			//would be nice to specify to compiler that the values in temp_array should be
            //undefined again and don't need to be retained from the previous iteration?
		}
		(*temp_current) = f(*in_current);
	}
	//flush data. Only case were it'll be empty is where num_vectors == 0, which shouldn't happen
	(*out_current++) = std::bit_cast<native_vector_type>(temp_data);

	return static_cast<size_t>(std::distance(vectors, out_current));
}
