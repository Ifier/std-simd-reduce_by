#include "reduce_by_impl.hpp"

#include <experimental/simd>
#include <cstddef>
#include <array>
#include <bit>
#include <algorithm>
#include <type_traits>

//apple clang has no deduce_t, so we need to supply our own
#if defined(__aarch64__)
#define REDUCE_BY_HAS_NO_DEDUCE_T 1
#endif

namespace std::experimental
{
	namespace
	{
		template<typename T, typename Abi>
		using native_array_type = array<native_vector_type, ((simd_size_v<T,Abi> * sizeof(T)) / sizeof(native_vector_type)) +
		                                                    ((simd_size_v<T,Abi> * sizeof(T)) % sizeof(native_vector_type) != 0)>;
		template<typename T, typename Abi>
		using value_array_type = array<T, sizeof(native_array_type<T,Abi>) / sizeof(T)>;

		template<typename T, typename Abi, T Fill>
		native_array_type<T, Abi> to_natives( simd<T, Abi> v )
		{
			alignas(simd<T, Abi>) value_array_type<T, Abi> values;
			v.copy_to(values.data(), vector_aligned);
			fill(values.begin() + v.size(), values.end(), Fill);
			return bit_cast<native_array_type<T, Abi>>(v);
		}
		template<typename T, typename Abi>
		simd<T, Abi> from_natives( native_array_type<T,Abi> v )
		{
			alignas(simd<T, Abi>) value_array_type<T, Abi> values = bit_cast<value_array_type<T, Abi>>(v);
			return simd<T, Abi>{ values.data(), vector_aligned };
		}
	};

#if REDUCE_BY_HAS_NO_DEDUCE_T
	namespace simd_abi
	{
		template<class T, size_t N, class... Abis>
		using deduce_t = conditional_t<N == simd_size_v<T, native<T>>, native<T>, fixed_size<N>>;
	};
#endif

	template<size_t N, typename U, typename T, typename Abi>
	using reduce_by_result_t = simd<U, simd_abi::deduce_t<U, (simd_size_v<T,Abi> / N) + (simd_size_v<T,Abi> % N != 0), Abi>>;

	template<size_t N, typename T, typename Abi, class F = std::plus<>>
	reduce_by_result_t<N,invoke_result_t<F, T, T>,T,Abi> reduce_by(const simd<T, Abi>& v, F f = F{}) noexcept
	{
		using in_type = T;
		using out_type = invoke_result_t<F, T, T>;

		using in_simd_type = simd<T, Abi>;
		using in_simd_abi = Abi;
		using out_simd_type = reduce_by_result_t<N,invoke_result_t<F, T, T>,T,Abi>;
		using out_simd_abi = typename out_simd_type::abi_type;

		constexpr VectorElementType k_in_element_type = get_vector_element_type<in_type>();
		constexpr VectorElementType k_out_element_type = get_vector_element_type<out_type>();

		constexpr size_t k_in_tsize = countr_zero(sizeof(in_type));
		constexpr size_t k_out_tsize = countr_zero(sizeof(out_type));

		constexpr size_t k_num_narrows = k_in_tsize > k_out_tsize ? k_in_tsize - k_out_tsize : 0;
		constexpr size_t k_num_widens = k_out_tsize > k_in_tsize ? k_out_tsize - k_in_tsize : 0;
		constexpr size_t k_num_reduces = countr_zero(N);

		using in_reduce_op = Simd_pairwise_reduce<F, k_in_tsize, k_in_element_type>;
		using out_reduce_op = Simd_pairwise_reduce<F, k_out_tsize, k_out_element_type>;
		using in_narrow_op = Simd_narrow<k_in_tsize, k_in_element_type, k_in_tsize - 1>;
		using in_widen_op = Simd_widen<k_in_tsize, k_in_element_type, k_in_tsize + 1>;

		constexpr bool k_has_required_ops = ( k_num_reduces == 0 || (in_reduce_op::k_supported && out_reduce_op::k_supported) ) &&
		                                    ( k_num_narrows == 0 || (in_narrow_op::k_supported) ) &&
		                                    ( k_num_widens == 0  || (in_widen_op::k_supported) );

		constexpr in_type k_fill = Simd_fill_type<F, in_type>::value;

		if constexpr ( has_single_bit(sizeof(in_type))  && in_reduce_op::k_supported &&
		               has_single_bit(sizeof(out_type)) && out_reduce_op::k_supported &&
		               has_single_bit(N) &&
		               k_has_required_ops )
		{
			using in_native_array = native_array_type<in_type, in_simd_abi>;
			using out_native_array = native_array_type<out_type, out_simd_abi>;

			constexpr size_t k_num_in_vectors = tuple_size_v<in_native_array>;
			constexpr size_t k_num_out_vectors = tuple_size_v<out_native_array>;
			constexpr size_t k_num_converted_vectors = k_num_in_vectors << k_num_widens;

			constexpr size_t k_num_peak_vectors = std::max({ k_num_in_vectors, k_num_out_vectors, k_num_converted_vectors });

			using working_native_array = array<native_vector_type, k_num_peak_vectors>;

			const in_native_array in_natives = to_natives<in_type, in_simd_abi, k_fill>(v);
			working_native_array working_natives;
			copy(in_natives.begin(), in_natives.end(), working_natives.begin());

			reduce_by_impl<F, k_in_tsize, k_in_element_type, k_out_element_type, k_num_narrows, k_num_widens, k_num_reduces>(working_natives.data(), in_natives.size());

			out_native_array out_natives;
			copy_n(working_natives.begin(), out_natives.size(), out_natives.begin());
			return from_natives<out_type, out_simd_abi>(out_natives);
		}
		else
		{
			alignas(in_simd_type) value_array_type<in_type, in_simd_abi> in_values;
			v.copy_to(in_values.data(), vector_aligned);
			in_type* in_current = in_values.data();
			in_type* const in_end = in_values.data() + (in_values.size() - (in_values.size() % N));

			alignas(out_simd_type) value_array_type<out_type, out_simd_abi> out_values;
			out_type* out_current = out_values.data();
			
			//make sure the type we accumulate is of out_type, not in_type, since they might
			//be of different sizes
			constexpr out_type k_reduce_base = k_fill;

			for (; in_current != in_end; in_current += N, ++out_current)
			{
				*out_current = std::reduce(in_current, in_current + N, k_reduce_base, f);
			}
			if constexpr (in_values.size() % N != 0)
			{
				*out_current = std::reduce(in_current, in_values.end(), k_reduce_base, f);
			}

			return out_simd_type{ out_values.data(), vector_aligned };
		}
	}

	template<size_t N, typename T, typename Abi>
	reduce_by_result_t<N,T,T,Abi> reduce_by_max( const simd<T, Abi>& v ) noexcept
	{
		return reduce_by<N>( v, vector_max<T>{} );
	}

	template<size_t N, typename T, typename Abi>
	reduce_by_result_t<N,T,T,Abi> reduce_by_min( const simd<T, Abi>& v) noexcept
	{
		return reduce_by<N>( v, vector_min<T>{} );
	}
};
