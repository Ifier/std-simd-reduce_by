#include "../operations.hpp"
#include <bit>
#include <functional>

#define DEFINE_OP(_name, _size, _type, _result, _params, _op)   \
template<typename T> struct _name<std::plus<T>, _size, _type> { \
static constexpr bool k_supported = true;                       \
REDUCE_BY_ALWAYS_INLINE _result operator() _params const noexcept { return _op; } }


#define DEFINE_FULL_REDUCE(_size, _type, _id)                                                 \
DEFINE_OP(Simd_full_reduce, _size, _type, element_type_bytes<_size>,                          \
(native_vector_type lhs),                                                                     \
std::bit_cast<element_type_bytes<_size>>( vaddvq_##_id( vreinterpretq_##_id##_u8( lhs ) ) ) )

DEFINE_FULL_REDUCE(0, VectorElementType::Unsigned_integer, u8);
DEFINE_FULL_REDUCE(1, VectorElementType::Unsigned_integer, u16);
DEFINE_FULL_REDUCE(2, VectorElementType::Unsigned_integer, u32);
DEFINE_FULL_REDUCE(3, VectorElementType::Unsigned_integer, u64);
DEFINE_FULL_REDUCE(0, VectorElementType::Signed_integer,   s8);
DEFINE_FULL_REDUCE(1, VectorElementType::Signed_integer,   s16);
DEFINE_FULL_REDUCE(2, VectorElementType::Signed_integer,   s32);
DEFINE_FULL_REDUCE(3, VectorElementType::Signed_integer,   s64);
DEFINE_FULL_REDUCE(2, VectorElementType::Float,            f32);
DEFINE_FULL_REDUCE(3, VectorElementType::Float,            f64);

#undef DEFINE_FULL_REDUCE

#define DEFINE_FULL_WIDEN_REDUCE(_size, _type, _id)                                                \
DEFINE_OP(Simd_full_widen_reduce, _size, _type, element_type_bytes<_size + 1>,                     \
(native_vector_type lhs),                                                                          \
std::bit_cast<element_type_bytes<_size + 1>>( vaddlvq_##_id( vreinterpretq_##_id##_u8( lhs ) ) ) )

DEFINE_FULL_WIDEN_REDUCE(0, VectorElementType::Unsigned_integer, u8);
DEFINE_FULL_WIDEN_REDUCE(1, VectorElementType::Unsigned_integer, u16);
DEFINE_FULL_WIDEN_REDUCE(2, VectorElementType::Unsigned_integer, u32);
DEFINE_FULL_WIDEN_REDUCE(0, VectorElementType::Signed_integer,   s8);
DEFINE_FULL_WIDEN_REDUCE(1, VectorElementType::Signed_integer,   s16);
DEFINE_FULL_WIDEN_REDUCE(2, VectorElementType::Signed_integer,   s32);

#undef DEFINE_FULL_WIDEN_REDUCE

#define DEFINE_PAIRWISE_REDUCE(_size, _type, _id)                                          \
DEFINE_OP(Simd_pairwise_reduce, _size, _type, native_vector_type,                          \
(native_vector_type lhs, native_vector_type rhs),                                          \
vreinterpretq_u8_##_id(                                                                  \
  vpaddq_##_id( vreinterpretq_##_id##_u8( lhs ), vreinterpretq_##_id##_u8( rhs ) ) ) )

DEFINE_PAIRWISE_REDUCE(0, VectorElementType::Unsigned_integer, u8);
DEFINE_PAIRWISE_REDUCE(1, VectorElementType::Unsigned_integer, u16);
DEFINE_PAIRWISE_REDUCE(2, VectorElementType::Unsigned_integer, u32);
DEFINE_PAIRWISE_REDUCE(3, VectorElementType::Unsigned_integer, u64);
DEFINE_PAIRWISE_REDUCE(0, VectorElementType::Signed_integer,   s8);
DEFINE_PAIRWISE_REDUCE(1, VectorElementType::Signed_integer,   s16);
DEFINE_PAIRWISE_REDUCE(2, VectorElementType::Signed_integer,   s32);
DEFINE_PAIRWISE_REDUCE(3, VectorElementType::Signed_integer,   s64);
DEFINE_PAIRWISE_REDUCE(2, VectorElementType::Float,            f32);
DEFINE_PAIRWISE_REDUCE(3, VectorElementType::Float,            f64);

#undef DEFINE_PAIRWISE_REDUCE

#define DEFINE_PAIRWISE_WIDEN_REDUCE(_size, _type, _fromid, _toid)                         \
DEFINE_OP(Simd_pairwise_widen_reduce, _size, _type, native_vector_type,                    \
(native_vector_type lhs),                                                                  \
vreinterpretq_u8_##_toid( vpaddlq_##_fromid( vreinterpretq_##_fromid##_u8( lhs ) ) ) )

DEFINE_PAIRWISE_WIDEN_REDUCE(0, VectorElementType::Unsigned_integer, u8, u16);
DEFINE_PAIRWISE_WIDEN_REDUCE(1, VectorElementType::Unsigned_integer, u16, u32);
DEFINE_PAIRWISE_WIDEN_REDUCE(2, VectorElementType::Unsigned_integer, u32, u64);
DEFINE_PAIRWISE_WIDEN_REDUCE(0, VectorElementType::Signed_integer,   s8, s16);
DEFINE_PAIRWISE_WIDEN_REDUCE(1, VectorElementType::Signed_integer,   s16, s32);
DEFINE_PAIRWISE_WIDEN_REDUCE(2, VectorElementType::Signed_integer,   s32, s64);

#undef DEFINE_PAIRWISE_WIDEN_REDUCE

#undef DEFINE_OP
