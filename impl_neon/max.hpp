#include "../operations.hpp"
#include <bit>
#include <algorithm>

#define DEFINE_OP(_name, _size, _type, _result, _params, _op)    \
template<typename T> struct _name<vector_max<T>, _size, _type> { \
static constexpr bool k_supported = true;                        \
REDUCE_BY_ALWAYS_INLINE _result operator() _params const noexcept { return _op; } }


#define DEFINE_FULL_REDUCE(_size, _type, _id)                                                 \
DEFINE_OP(Simd_full_reduce, _size, _type, element_type_bytes<_size>,                          \
(native_vector_type lhs),                                                                     \
std::bit_cast<element_type_bytes<_size>>( vmaxvq_##_id( vreinterpretq_##_id##_u8( lhs ) ) ) )

DEFINE_FULL_REDUCE(0, VectorElementType::Unsigned_integer, u8);
DEFINE_FULL_REDUCE(1, VectorElementType::Unsigned_integer, u16);
DEFINE_FULL_REDUCE(2, VectorElementType::Unsigned_integer, u32);
//DEFINE_FULL_REDUCE(3, VectorElementType::Unsigned_integer, u64);
DEFINE_FULL_REDUCE(0, VectorElementType::Signed_integer,   s8);
DEFINE_FULL_REDUCE(1, VectorElementType::Signed_integer,   s16);
DEFINE_FULL_REDUCE(2, VectorElementType::Signed_integer,   s32);
//DEFINE_FULL_REDUCE(3, VectorElementType::Signed_integer,   s64);
DEFINE_FULL_REDUCE(2, VectorElementType::Float,            f32);
DEFINE_FULL_REDUCE(3, VectorElementType::Float,            f64);

//arm has instruction set holes for horizontal 64 bit integer operations, so these need to be defined seperately
template<typename T> struct Simd_full_reduce<vector_max<T>, 3, VectorElementType::Unsigned_integer>
{
        static constexpr bool k_supported = true;
        REDUCE_BY_ALWAYS_INLINE element_type_bytes<3> operator()(native_vector_type lhs) const noexcept
        {
			const uint64x1_t low = vget_low_u64(vreinterpretq_u64_u8(lhs));
			const uint64x1_t high = vget_high_u64(vreinterpretq_u64_u8(lhs));
			return std::bit_cast<element_type_bytes<3>>( vbsl_u64( vcgt_u64(low, high), low, high ) );
        }
};
template<typename T> struct Simd_full_reduce<vector_max<T>, 3, VectorElementType::Signed_integer>
{
        static constexpr bool k_supported = true;
        REDUCE_BY_ALWAYS_INLINE element_type_bytes<3> operator()(native_vector_type lhs) const noexcept
        {
			const int64x1_t low = vget_low_s64(vreinterpretq_s64_u8(lhs));
			const int64x1_t high = vget_high_s64(vreinterpretq_s64_u8(lhs));
			return std::bit_cast<element_type_bytes<3>>( vbsl_s64( vcgt_s64(low, high), low, high ) );
        }
};

#undef DEFINE_FULL_REDUCE

#define DEFINE_PAIRWISE_REDUCE(_size, _type, _id)                                          \
DEFINE_OP(Simd_pairwise_reduce, _size, _type, native_vector_type,                          \
(native_vector_type lhs, native_vector_type rhs),                                          \
vreinterpretq_u8_##_id(                                                                    \
  vpmaxq_##_id( vreinterpretq_##_id##_u8( lhs ), vreinterpretq_##_id##_u8( rhs ) ) ) )

DEFINE_PAIRWISE_REDUCE(0, VectorElementType::Unsigned_integer, u8);
DEFINE_PAIRWISE_REDUCE(1, VectorElementType::Unsigned_integer, u16);
DEFINE_PAIRWISE_REDUCE(2, VectorElementType::Unsigned_integer, u32);
//DEFINE_PAIRWISE_REDUCE(3, VectorElementType::Unsigned_integer, u64);
DEFINE_PAIRWISE_REDUCE(0, VectorElementType::Signed_integer,   s8);
DEFINE_PAIRWISE_REDUCE(1, VectorElementType::Signed_integer,   s16);
DEFINE_PAIRWISE_REDUCE(2, VectorElementType::Signed_integer,   s32);
//DEFINE_PAIRWISE_REDUCE(3, VectorElementType::Signed_integer,   s64);
DEFINE_PAIRWISE_REDUCE(2, VectorElementType::Float,            f32);
DEFINE_PAIRWISE_REDUCE(3, VectorElementType::Float,            f64);

//arm has instruction set holes for horizontal 64 bit integer operations, so these need to be defined seperately
template<typename T> struct Simd_pairwise_reduce<vector_max<T>, 3, VectorElementType::Unsigned_integer>
{
        static constexpr bool k_supported = true;
        REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
        {
                const uint64x2_t low_halves  = vzip1q_u64(vreinterpretq_u64_u8(lhs), vreinterpretq_u64_u8(rhs));
                const uint64x2_t high_halves = vzip2q_u64(vreinterpretq_u64_u8(lhs), vreinterpretq_u64_u8(rhs));
                return vreinterpretq_u8_u64(vbslq_u64(vcgtq_u64(low_halves, high_halves), low_halves, high_halves));
        }
};
template<typename T> struct Simd_pairwise_reduce<vector_max<T>, 3, VectorElementType::Signed_integer>
{
        static constexpr bool k_supported = true;
        REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
        {
                const int64x2_t low_halves  = vzip1q_s64(vreinterpretq_s64_u8(lhs), vreinterpretq_s64_u8(rhs));
                const int64x2_t high_halves = vzip2q_s64(vreinterpretq_s64_u8(lhs), vreinterpretq_s64_u8(rhs));
                return vreinterpretq_u8_s64(vbslq_s64(vcgtq_s64(low_halves, high_halves), low_halves, high_halves));
        }
};

#undef DEFINE_PAIRWISE_REDUCE

#undef DEFINE_OP
