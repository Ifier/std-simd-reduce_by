#include "../operations.hpp"

#define DEFINE_OP(_name, _size, _type, _result, _params, _op, ...) \
template<> struct _name<_size, _type __VA_OPT__(,) __VA_ARGS__> {  \
static constexpr bool k_supported = true;                          \
_result operator() _params const noexcept { return _op; } }

#define DEFINE_NARROW(_size, _type, _fromid, _toid, _intrin)     \
DEFINE_OP(Simd_narrow, _size, _type, native_vector_type,         \
(vector_array<2> lhs),                                           \
vreinterpretq_u8_##_toid(vcombine_##_toid(                       \
  _intrin##_##_fromid( vreinterpretq_##_fromid##_u8(lhs[0]) ),   \
  _intrin##_##_fromid( vreinterpretq_##_fromid##_u8(lhs[1]) ))), \
_size - 1)

DEFINE_NARROW(1, VectorElementType::Unsigned_integer, u16, u8, vmovn);
DEFINE_NARROW(2, VectorElementType::Unsigned_integer, u32, u16, vmovn);
DEFINE_NARROW(3, VectorElementType::Unsigned_integer, u64, u32, vmovn);
DEFINE_NARROW(1, VectorElementType::Signed_integer,   s16, s8, vmovn);
DEFINE_NARROW(2, VectorElementType::Signed_integer,   s32, s16, vmovn);
DEFINE_NARROW(3, VectorElementType::Signed_integer,   s64, s32, vmovn);
DEFINE_NARROW(3, VectorElementType::Float,            f64, f32, vcvt_f32);

#undef DEFINE_NARROW

#define DEFINE_WIDEN(_size, _type, _fromid, _toid, _intrin)                                                  \
DEFINE_OP(Simd_widen, _size, _type, vector_array<2>,                                                         \
(native_vector_type lhs), vector_array<2>({                                                                  \
vreinterpretq_u8_##_toid( _intrin##_##_fromid( vget_low_##_fromid(  vreinterpretq_##_fromid##_u8(lhs) ) ) ), \
vreinterpretq_u8_##_toid( _intrin##_##_fromid( vget_high_##_fromid( vreinterpretq_##_fromid##_u8(lhs) ) ) )  \
}), _size + 1)

DEFINE_WIDEN(0, VectorElementType::Unsigned_integer, u8, u16,  vmovl);
DEFINE_WIDEN(1, VectorElementType::Unsigned_integer, u16, u32, vmovl);
DEFINE_WIDEN(2, VectorElementType::Unsigned_integer, u32, u64, vmovl);
DEFINE_WIDEN(0, VectorElementType::Signed_integer,   s8, s16,  vmovl);
DEFINE_WIDEN(1, VectorElementType::Signed_integer,   s16, s32, vmovl);
DEFINE_WIDEN(2, VectorElementType::Signed_integer,   s32, s64, vmovl);
DEFINE_WIDEN(2, VectorElementType::Float,            f32, f64, vcvt_f64);

#undef DEFINE_WIDEN

#define DEFINE_CONVERT(_size, _fromtype, _totype, _fromid, _toid)                            \
DEFINE_OP(Simd_convert, _size, _fromtype, native_vector_type,                                \
(native_vector_type lhs),                                                                    \
vreinterpretq_u8_##_toid( vcvtq_##_toid##_##_fromid( vreinterpretq_##_fromid##_u8(lhs) ) ),  \
_totype)

DEFINE_CONVERT(2, VectorElementType::Unsigned_integer, VectorElementType::Float,            u32, f32);
DEFINE_CONVERT(3, VectorElementType::Unsigned_integer, VectorElementType::Float,            u64, f64);
DEFINE_CONVERT(2, VectorElementType::Signed_integer,   VectorElementType::Float,            s32, f32);
DEFINE_CONVERT(3, VectorElementType::Signed_integer,   VectorElementType::Float,            s64, f64);
DEFINE_CONVERT(2, VectorElementType::Float,            VectorElementType::Unsigned_integer, f32, u32);
DEFINE_CONVERT(3, VectorElementType::Float,            VectorElementType::Unsigned_integer, f64, u64);
DEFINE_CONVERT(2, VectorElementType::Float,            VectorElementType::Signed_integer,   f32, s32);
DEFINE_CONVERT(3, VectorElementType::Float,            VectorElementType::Signed_integer,   f64, s64);

#undef DEFINE_CONVERT

#undef DEFINE_OP
