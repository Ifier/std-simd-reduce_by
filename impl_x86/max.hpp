#include "../operations.hpp"

#include <bit>

//TODO: evaulate if these are actually faster than repeated pairwise operations
//we don't need to support full reduce so we can get rid of them

template<typename T>
struct Simd_full_reduce<vector_max<T>, 0, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	element_type_bytes<0> operator()(native_vector_type lhs) const noexcept
	{
	        //we can still use horizontal min by negating the inputs
	        const native_vector_type inverted = _mm_sub_epi8(_mm_setzero_si128(), lhs);
	        
		//using a horizontal min instead of reducing from u8 to u64 allows more instructions
		//to be done in parallel, even though it's the same amount of instructions
		const native_vector_type low = _mm_slli_epi16(inverted, 8);
		const native_vector_type high = _mm_srli_epi16(inverted, 8);
		const native_vector_type low_min = _mm_srli_epi16(_mm_minpos_epu16(low), 8);
		const native_vector_type high_min = _mm_minpos_epu16(high);
		const native_vector_type min = _mm_min_epu8(low_min, high_min);
		
		//undo negation
		const native_vector_type max = _mm_sub_epi8(_mm_setzero_si128(), min);
		
		return std::bit_cast<element_type_bytes<0>>(static_cast<uint8_t>(std::bit_cast<std::array<uint64_t,2>>(max)[0]));
	}
};
template<typename T>
struct Simd_full_reduce<vector_max<T>, 1, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	element_type_bytes<1> operator()(native_vector_type lhs) const noexcept
	{
	        //negation trick as above
	        const native_vector_type inverted = _mm_sub_epi16(_mm_setzero_si128(), lhs);
	        const native_vector_type min = _mm_minpos_epu16(inverted);
	        const native_vector_type max = _mm_sub_epi16(_mm_setzero_si128(), min);
	        
		//this is the one specific case where we have a horizontal min
		return std::bit_cast<element_type_bytes<1>>(static_cast<uint16_t>(std::bit_cast<std::array<uint64_t,2>>(max)[0]));
	}
};
/*template<typename T>
struct Simd_full_reduce<vector_max<T>, 2, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	element_type_bytes<2> operator()(native_vector_type lhs) const noexcept
	{
		const native_vector_type shifted_u32 = _mm_srli_epi64(lhs, 32);
		const native_vector_type max_u64 = _mm_max_epu32(lhs, shifted_u32);
		
		return std::bit_cast<element_type_bytes<2>>(static_cast<uint32_t>(std::bit_cast<std::array<uint64_t,2>>(max)[0]));
	}
};*/
template<typename T>
struct Simd_full_reduce<vector_max<T>, 3, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	element_type_bytes<3> operator()(native_vector_type lhs) const noexcept
	{
	        //can just swap order of operands here
		const native_vector_type sign_bit = _mm_set1_epi64( std::bit_cast<__m64>(0x8000000000000000ull) );
		const native_vector_type lhs_high = _mm_movehl_ps(lhs, lhs);
		const native_vector_type max = _mm_cmpgt_epi64(_mm_sub_epi64(lhs_high, sign_bit), _mm_sub_epi64(lhs, sign_bit));
		return std::bit_cast<element_type_bytes<3>>(std::bit_cast<std::array<uint64_t,2>>(_mm_blendv_epi8(lhs, lhs_high, max))[0]);
	}
};

//don't have the mental capacity to work out how to do these right now
/*template<typename T>
struct Simd_full_reduce<vector_max<T>, 0, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	element_type_bytes<0> operator()(native_vector_type lhs) const noexcept
	{
		//xor the sign bit - non-negative numbers will become larger than negative numbers
		//and negative numbers will become smaller than non-negative numbers. Relative ordering
		//between numbers of the same signedness is kept the same
		const native_vector_type sign_bit = _mm_set1_epi8(0x80);
		
		//using a horizontal min instead of reducing from u8 to u64 allows more instructions
		//to be done in parallel, even though it's the same amount of instructions
		const native_vector_type low = _mm_slli_epi16(_mm_xor_si128(lhs, sign_bit), 8);
		const native_vector_type high = _mm_srli_epi16(_mm_xor_si128(lhs, sign_bit), 8);
		const native_vector_type low_min = _mm_srli_epi16(_mm_minpos_epu16(low), 8);
		const native_vector_type high_min = _mm_minpos_epu16(high);
		//undo the xoring of the sign bit
		const native_vector_type min = _mm_min_epu8(low_min, high_min), sign_bit);
		
		return std::bit_cast<element_type_bytes<0>>(static_cast<uint8_t>(std::bit_cast<std::array<uint64_t,2>>(min)[0]));
	}
};
template<typename T>
struct Simd_full_reduce<vector_max<T>, 1, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	element_type_bytes<1> operator()(native_vector_type lhs) const noexcept
	{
		//xor sign bit trick, as above
		const native_vector_type sign_bit = _mm_set1_epi8(0x80);
		//this is the one specific case where we have a horizontal min
		const native_vector_type min = _mm_xor_si128(_mm_minpos_epu16(_mm_xor_si128(lhs, sign_bit)), sign_bit);
		
		return std::bit_cast<element_type_bytes<1>>(static_cast<uint16_t>(std::bit_cast<std::array<uint64_t,2>>(min)[0]));
	}
};
template<typename T>
struct Simd_full_reduce<vector_max<T>, 2, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	element_type_bytes<2> operator()(native_vector_type lhs) const noexcept
	{
		const native_vector_type shifted_s32 = _mm_srai_epi64(lhs, 32);
		const native_vector_type max_s64 = _mm_max_epi32(lhs, shifted_s32);
		
		return std::bit_cast<element_type_bytes<2>>(static_cast<uint32_t>(std::bit_cast<std::array<uint64_t,2>>(max)[0]));
	}
};*/
template<typename T>
struct Simd_full_reduce<vector_max<T>, 3, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	element_type_bytes<3> operator()(native_vector_type lhs) const noexcept
	{
		const native_vector_type lhs_high = _mm_movehl_ps(lhs, lhs);
		const native_vector_type max = _mm_cmpgt_epi64(lhs, lhs_high);
		return std::bit_cast<element_type_bytes<3>>(std::bit_cast<std::array<uint64_t,2>>(_mm_blendv_epi8(lhs, lhs_high, max))[0]);
	}
};

//unsigned pairwise min 16/32 bit is thankfully fairly straightforward! We can narrow in a single instruction so long
//as the upper element is zero, which since we have to shift it down to line up the pairs, it is
template<typename T>
struct Simd_pairwise_reduce<vector_max<T>, 0, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        const native_vector_type low_mask = _mm_set1_epi16(0x00FF);
		const native_vector_type lhs_shifted = _mm_srli_epi16(lhs, 8);
		const native_vector_type lhs_max = _mm_max_epu8(_mm_and_si128(lhs, low_mask), lhs_shifted);
		const native_vector_type rhs_shifted = _mm_srli_epi16(rhs, 8);
		const native_vector_type rhs_max = _mm_max_epu8(_mm_and_si128(rhs, low_mask), rhs_shifted);
		return _mm_packus_epi16(lhs_max, rhs_max);
	}
};
template<typename T>
struct Simd_pairwise_reduce<vector_max<T>, 1, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        const native_vector_type low_mask = _mm_set1_epi32(0x0000FFFF);
		const native_vector_type lhs_shifted = _mm_srli_epi32(lhs, 16);
		const native_vector_type lhs_max = _mm_max_epu16(_mm_and_si128(lhs, low_mask), lhs_shifted);
		const native_vector_type rhs_shifted = _mm_srli_epi32(rhs, 16);
		const native_vector_type rhs_max = _mm_max_epu16(_mm_and_si128(rhs, low_mask), rhs_shifted);
		return _mm_packus_epi32(lhs_max, rhs_max);
	}
};
template<typename T>
struct Simd_pairwise_reduce<vector_max<T>, 0, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        //since we need to have the top 8 bits be zero we need to mask them out in the non-shifted
	        //values since they would maybe be greater than zero
	        const native_vector_type low_mask = _mm_set1_epi16(0x00FF);
		const native_vector_type lhs_shifted = _mm_srli_epi16(lhs, 8);
		const native_vector_type lhs_masked = _mm_and_si128(lhs, low_mask);
		const native_vector_type lhs_max = _mm_max_epi8(lhs_masked, lhs_shifted);
		const native_vector_type rhs_shifted = _mm_srli_epi16(rhs, 8);
		const native_vector_type rhs_masked = _mm_and_si128(rhs, low_mask);
		const native_vector_type rhs_max = _mm_max_epi8(rhs_masked, rhs_shifted);
		return _mm_packus_epi16(lhs_max, rhs_max);
	}
};
template<typename T>
struct Simd_pairwise_reduce<vector_max<T>, 1, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        const native_vector_type low_mask = _mm_set1_epi32(0x0000FFFF);
		const native_vector_type lhs_shifted = _mm_srli_epi32(lhs, 16);
		const native_vector_type lhs_max = _mm_max_epi16(_mm_and_si128(lhs, low_mask), lhs_shifted);
		const native_vector_type rhs_shifted = _mm_srli_epi32(rhs, 16);
		const native_vector_type rhs_max = _mm_max_epi16(_mm_and_si128(rhs, low_mask), rhs_shifted);
		return _mm_packus_epi32(lhs_max, rhs_max);
	}
};

//for 32 bit it gets slightly more compilcated, since we have to use a ps-only shuffle method to
//extract the min elements
template<typename T>
struct Simd_pairwise_reduce<vector_max<T>, 2, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
		const native_vector_type lhs_shifted = _mm_srli_epi64(lhs, 32);
		const native_vector_type lhs_max = _mm_max_epu32(lhs, lhs_shifted);
		const native_vector_type rhs_shifted = _mm_srli_epi64(rhs, 32);
		const native_vector_type rhs_max = _mm_max_epu32(rhs, rhs_shifted);
		const __m128 lhs_as_float = std::bit_cast<__m128>(lhs_max);
		const __m128 rhs_as_float = std::bit_cast<__m128>(rhs_max);
		constexpr int k_control_mask = 0b10001000; // C = { A[0,2], B[0,2] }
		return std::bit_cast<native_vector_type>(_mm_shuffle_ps(lhs_as_float, rhs_as_float, k_control_mask));
	}
};
template<typename T>
struct Simd_pairwise_reduce<vector_max<T>, 2, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
		const native_vector_type lhs_shifted = _mm_srli_epi64(lhs, 32);
		const native_vector_type lhs_max = _mm_max_epi32(lhs, lhs_shifted);
		const native_vector_type rhs_shifted = _mm_srli_epi64(rhs, 32);
		const native_vector_type rhs_max = _mm_max_epi32(rhs, rhs_shifted);
		const __m128 lhs_as_float = std::bit_cast<__m128>(lhs_max);
		const __m128 rhs_as_float = std::bit_cast<__m128>(rhs_max);
		constexpr int k_control_mask = 0b10001000; // C = { A[0,2], B[0,2] }
		return std::bit_cast<native_vector_type>(_mm_shuffle_ps(lhs_as_float, rhs_as_float, k_control_mask));
	}
};

//64 bit is completely different. Like Arm, no 64 bit min operations so we have to do a manual compare-select
template<typename T>
struct Simd_pairwise_reduce<vector_max<T>, 3, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
		const native_vector_type sign_bit = _mm_set1_epi64( std::bit_cast<__m64>(0x8000000000000000ull) );
		const native_vector_type low_halves = _mm_movelh_ps(lhs, rhs);
		const native_vector_type high_halves = _mm_movehl_ps(rhs, lhs);
		//worse than Arm: there's no 64 bit unsigned compare operations either!
		const native_vector_type max = _mm_cmpgt_epi64(_mm_xor_si128(low_halves, sign_bit), _mm_xor_si128(high_halves, sign_bit));
		return _mm_blendv_epi8(high_halves, low_halves, max);
	}
};
template<typename T>
struct Simd_pairwise_reduce<vector_max<T>, 3, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
		const native_vector_type low_halves = _mm_movelh_ps(lhs, rhs);
		const native_vector_type high_halves = _mm_movehl_ps(rhs, lhs);
		const native_vector_type max = _mm_cmpgt_epi64(low_halves, high_halves);
		return _mm_blendv_epi8(high_halves, low_halves, max);
	}
};

//not making full reductions for floats but I can do pairwise reductions
template<typename T>
struct Simd_pairwise_reduce<vector_max<T>, 2, VectorElementType::Float>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
		const native_vector_type lhs_shifted = _mm_slli_epi64(lhs, 32);
		const __m128 lhs_max = _mm_max_ps(std::bit_cast<__m128>(lhs), std::bit_cast<__m128>(lhs_shifted));
		const native_vector_type rhs_shifted = _mm_slli_epi64(rhs, 32);
		const __m128 rhs_max = _mm_max_ps(std::bit_cast<__m128>(rhs), std::bit_cast<__m128>(rhs_shifted));
		constexpr int k_control_mask = 0b11011101; // C = { A[1,3], B[1,3] }
		return std::bit_cast<native_vector_type>(_mm_shuffle_ps(lhs_max, rhs_max, k_control_mask));
	}
};
template<typename T>
struct Simd_pairwise_reduce<vector_max<T>, 3, VectorElementType::Float>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        const __m128 lhs_high = _mm_movehl_ps(_mm_setzero_pd(), std::bit_cast<__m128>(lhs));
	        const __m128 rhs_high = _mm_movehl_ps(_mm_setzero_pd(), std::bit_cast<__m128>(rhs));
	        const __m128 lhs_max = _mm_max_pd(std::bit_cast<__m128>(lhs), lhs_high);
	        const __m128 rhs_max = _mm_max_pd(std::bit_cast<__m128>(rhs), rhs_high);
	        return std::bit_cast<native_vector_type>(_mm_movelh_ps(rhs_max, lhs_max));
	}
};
