#include "../operations.hpp"

#include <cstdint>
#include <algorithm>

template<>
struct Simd_narrow<1, VectorElementType::Unsigned_integer, 0>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(vector_array<2> lhs) const noexcept
	{
		const native_vector_type even_mask = _mm_set1_epi16(0x00FF);
		const native_vector_type lhs_truncated = _mm_and_si128(lhs[0], even_mask);
		const native_vector_type rhs_truncated = _mm_and_si128(lhs[1], even_mask);
		return _mm_packus_epi16(lhs_truncated, rhs_truncated);
	}
};
template<>
struct Simd_narrow<2, VectorElementType::Unsigned_integer, 1>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(vector_array<2> lhs) const noexcept
	{
		constexpr int k_control_mask = 0b10101010; // C = { A[0],B[1],A[2],B[3],A[4],B[5],A[6],B[7] }
		const native_vector_type zeros = _mm_setzero_si128();
		const native_vector_type lhs_truncated = _mm_blend_epi16(lhs[0], zeros, k_control_mask);
		const native_vector_type rhs_truncated = _mm_blend_epi16(lhs[1], zeros, k_control_mask);
		return _mm_packus_epi32(lhs_truncated, rhs_truncated);
	}
};
template<>
struct Simd_narrow<3, VectorElementType::Unsigned_integer, 2>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(vector_array<2> lhs) const noexcept
	{
		const __m128 lhs_as_float = std::bit_cast<__m128>(lhs[0]);
		const __m128 rhs_as_float = std::bit_cast<__m128>(lhs[1]);
		constexpr int k_control_mask = 0b10001000; // C = { A[0,2], B[0,2] }
		return std::bit_cast<native_vector_type>(_mm_shuffle_ps(lhs_as_float, rhs_as_float, k_control_mask));
	}
};
template<>
struct Simd_narrow<1, VectorElementType::Signed_integer, 0>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(vector_array<2> lhs) const noexcept
	{
		const native_vector_type even_mask = _mm_set1_epi16(0x00FF);
		const native_vector_type lhs_truncated = _mm_and_si128(lhs[0], even_mask);
		const native_vector_type rhs_truncated = _mm_and_si128(lhs[1], even_mask);
		return _mm_packus_epi16(lhs_truncated, rhs_truncated);
	}
};
template<>
struct Simd_narrow<2, VectorElementType::Signed_integer, 1>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(vector_array<2> lhs) const noexcept
	{
		constexpr int k_control_mask = 0b10101010; // C = { A[0],B[1],A[2],B[3],A[4],B[5],A[6],B[7] }
		const native_vector_type zeros = _mm_setzero_si128();
		const native_vector_type lhs_truncated = _mm_blend_epi16(lhs[0], zeros, k_control_mask);
		const native_vector_type rhs_truncated = _mm_blend_epi16(lhs[1], zeros, k_control_mask);
		return _mm_packus_epi32(lhs_truncated, rhs_truncated);
	}
};
template<>
struct Simd_narrow<3, VectorElementType::Signed_integer, 2>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(vector_array<2> lhs) const noexcept
	{
		const __m128 lhs_as_float = std::bit_cast<__m128>(lhs[0]);
		const __m128 rhs_as_float = std::bit_cast<__m128>(lhs[1]);
		constexpr int k_control_mask = 0b10001000; // C = { A[0,2], B[0,2] }
		return std::bit_cast<native_vector_type>(_mm_shuffle_ps(lhs_as_float, rhs_as_float, k_control_mask));
	}
};
template<>
struct Simd_narrow<3, VectorElementType::Float, 2>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(vector_array<2> lhs) const noexcept
	{
		const __m128 lhs_narrowed = _mm_cvtpd_ps(std::bit_cast<__m128>(lhs[0]));
		const __m128 rhs_narrowed = _mm_cvtpd_ps(std::bit_cast<__m128>(lhs[1]));
		constexpr int k_control_mask = 0b01000100; // C = { A[0,1], B[0,1] }
		return std::bit_cast<native_vector_type>(_mm_shuffle_ps(lhs_narrowed, rhs_narrowed, k_control_mask));
	}
};

template<>
struct Simd_widen<0, VectorElementType::Unsigned_integer, 1>
{
	static constexpr bool k_supported = true;
	vector_array<2> operator()(native_vector_type lhs) const noexcept
	{
		return { _mm_cvtepu8_epi16(lhs), _mm_cvtepu8_epi16(_mm_movehl_ps(lhs,lhs)) };
	}
};
template<>
struct Simd_widen<0, VectorElementType::Unsigned_integer, 2>
{
	static constexpr bool k_supported = true;
	vector_array<4> operator()(native_vector_type lhs) const noexcept
	{
		return
		{
			_mm_cvtepu8_epi32(                    lhs      ),
			_mm_cvtepu8_epi32( _mm_shuffle_epi32( lhs, 1 ) ),
			_mm_cvtepu8_epi32( _mm_shuffle_epi32( lhs, 2 ) ),
			_mm_cvtepu8_epi32( _mm_shuffle_epi32( lhs, 3 ) )
		};
	}
};
template<>
struct Simd_widen<0, VectorElementType::Unsigned_integer, 3>
{
	static constexpr bool k_supported = true;
	vector_array<8> operator()(native_vector_type lhs) const noexcept
	{
		const std::array<native_vector_type, 4> quartets
		{
			lhs,
			_mm_shuffle_epi32(lhs, 1),
			_mm_shuffle_epi32(lhs, 2),
			_mm_shuffle_epi32(lhs, 3)
		};
		return
		{
			_mm_cvtepu8_epi64(                quartets[0]      ),
			_mm_cvtepu8_epi64( _mm_srli_epi32(quartets[0], 16) ),
			_mm_cvtepu8_epi64(                quartets[1]      ),
			_mm_cvtepu8_epi64( _mm_srli_epi32(quartets[1], 16) ),
			_mm_cvtepu8_epi64(                quartets[2]      ),
			_mm_cvtepu8_epi64( _mm_srli_epi32(quartets[2], 16) ),
			_mm_cvtepu8_epi64(                quartets[3]      ),
			_mm_cvtepu8_epi64( _mm_srli_epi32(quartets[3], 16) ),
		};
	}
};
template<>
struct Simd_widen<1, VectorElementType::Unsigned_integer, 2>
{
	static constexpr bool k_supported = true;
	vector_array<2> operator()(native_vector_type lhs) const noexcept
	{
		return { _mm_cvtepu16_epi32(lhs), _mm_cvtepu16_epi32(_mm_movehl_ps(lhs,lhs)) };
	}
};
template<>
struct Simd_widen<1, VectorElementType::Unsigned_integer, 3>
{
	static constexpr bool k_supported = true;
	vector_array<4> operator()(native_vector_type lhs) const noexcept
	{
		return
		{
			_mm_cvtepu8_epi64(                    lhs      ),
			_mm_cvtepu8_epi64( _mm_shuffle_epi32( lhs, 1 ) ),
			_mm_cvtepu8_epi64( _mm_shuffle_epi32( lhs, 2 ) ),
			_mm_cvtepu8_epi64( _mm_shuffle_epi32( lhs, 3 ) )
		};
	}
};
template<>
struct Simd_widen<2, VectorElementType::Unsigned_integer, 3>
{
	static constexpr bool k_supported = true;
	vector_array<2> operator()(native_vector_type lhs) const noexcept
	{
		return { _mm_cvtepu32_epi64(lhs), _mm_cvtepu32_epi64(_mm_movehl_ps(lhs,lhs)) };
	}
};
template<>
struct Simd_widen<0, VectorElementType::Signed_integer, 1>
{
	static constexpr bool k_supported = true;
	vector_array<2> operator()(native_vector_type lhs) const noexcept
	{
		return { _mm_cvtepi8_epi16(lhs), _mm_cvtepi8_epi16(_mm_movehl_ps(lhs,lhs)) };
	}
};
template<>
struct Simd_widen<0, VectorElementType::Signed_integer, 2>
{
	static constexpr bool k_supported = true;
	vector_array<4> operator()(native_vector_type lhs) const noexcept
	{
		return
		{
			_mm_cvtepi8_epi32(                    lhs      ),
			_mm_cvtepi8_epi32( _mm_shuffle_epi32( lhs, 1 ) ),
			_mm_cvtepi8_epi32( _mm_shuffle_epi32( lhs, 2 ) ),
			_mm_cvtepi8_epi32( _mm_shuffle_epi32( lhs, 3 ) )
		};
	}
};
template<>
struct Simd_widen<0, VectorElementType::Signed_integer, 3>
{
	static constexpr bool k_supported = true;
	vector_array<8> operator()(native_vector_type lhs) const noexcept
	{
		const std::array<native_vector_type, 4> quartets
		{
			lhs,
			_mm_shuffle_epi32(lhs, 1),
			_mm_shuffle_epi32(lhs, 2),
			_mm_shuffle_epi32(lhs, 3)
		};
		return
		{
			_mm_cvtepi8_epi64(                quartets[0]      ),
			_mm_cvtepi8_epi64( _mm_srli_epi32(quartets[0], 16) ),
			_mm_cvtepi8_epi64(                quartets[1]      ),
			_mm_cvtepi8_epi64( _mm_srli_epi32(quartets[1], 16) ),
			_mm_cvtepi8_epi64(                quartets[2]      ),
			_mm_cvtepi8_epi64( _mm_srli_epi32(quartets[2], 16) ),
			_mm_cvtepi8_epi64(                quartets[3]      ),
			_mm_cvtepi8_epi64( _mm_srli_epi32(quartets[3], 16) ),
		};
	}
};
template<>
struct Simd_widen<1, VectorElementType::Signed_integer, 2>
{
	static constexpr bool k_supported = true;
	vector_array<2> operator()(native_vector_type lhs) const noexcept
	{
		return { _mm_cvtepi16_epi32(lhs), _mm_cvtepi16_epi32(_mm_movehl_ps(lhs,lhs)) };
	}
};
template<>
struct Simd_widen<1, VectorElementType::Signed_integer, 3>
{
	static constexpr bool k_supported = true;
	vector_array<4> operator()(native_vector_type lhs) const noexcept
	{
		return
		{
			_mm_cvtepi8_epi64(                    lhs      ),
			_mm_cvtepi8_epi64( _mm_shuffle_epi32( lhs, 1 ) ),
			_mm_cvtepi8_epi64( _mm_shuffle_epi32( lhs, 2 ) ),
			_mm_cvtepi8_epi64( _mm_shuffle_epi32( lhs, 3 ) )
		};
	}
};
template<>
struct Simd_widen<2, VectorElementType::Signed_integer, 3>
{
	static constexpr bool k_supported = true;
	vector_array<2> operator()(native_vector_type lhs) const noexcept
	{
		return { _mm_cvtepi32_epi64(lhs), _mm_cvtepi32_epi64(_mm_movehl_ps(lhs,lhs)) };
	}
};
template<>
struct Simd_widen<2, VectorElementType::Float, 3>
{
	static constexpr bool k_supported = true;
	vector_array<2> operator()(native_vector_type lhs) const noexcept
	{
		const __m128 widened_low  = _mm_cvtps_pd(std::bit_cast<__m128>(lhs));
		const __m128 widened_high = _mm_cvtps_pd(std::bit_cast<__m128>(_mm_movehl_ps(lhs,lhs)));
		return { std::bit_cast<native_vector_type>(widened_low), std::bit_cast<native_vector_type>(widened_high) };
	}
};

//x86 without AVX512 only has int32 -> float, and I don't know enough about floating point
//numbers to be able to evaluate a better conversion method
template<size_t N, typename T, typename U>
inline native_vector_type scalar_convert(native_vector_type lhs) noexcept
{
	using in_array = std::array<T,N>;
	using out_array = std::array<U,N>;
	
	const in_array values_in = std::bit_cast<in_array>(lhs);
	out_array values_out;
	std::transform(values_in.cbegin(), values_in.cend(), values_out.begin(), [](T in){ return static_cast<U>(in); });
	return std::bit_cast<native_vector_type>(values_out);
}

template<>
struct Simd_convert<2, VectorElementType::Unsigned_integer, VectorElementType::Float>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs) const noexcept
	{
		return scalar_convert<4,uint32_t,float>(lhs);
	}
};
template<>
struct Simd_convert<3, VectorElementType::Unsigned_integer, VectorElementType::Float>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs) const noexcept
	{
		return scalar_convert<2,uint64_t,double>(lhs);
	}
};
template<>
struct Simd_convert<2, VectorElementType::Signed_integer, VectorElementType::Float>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs) const noexcept
	{
		return std::bit_cast<native_vector_type>(_mm_cvtepi32_ps(lhs));
	}
};
template<>
struct Simd_convert<3, VectorElementType::Signed_integer, VectorElementType::Float>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs) const noexcept
	{
		return scalar_convert<2,int64_t,double>(lhs);
	}
};
template<>
struct Simd_convert<2, VectorElementType::Float, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs) const noexcept
	{
		return scalar_convert<4,float,uint32_t>(lhs);
	}
};
template<>
struct Simd_convert<3, VectorElementType::Float, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs) const noexcept
	{
		return scalar_convert<2,double,uint64_t>(lhs);
	}
};
template<>
struct Simd_convert<2, VectorElementType::Float, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs) const noexcept
	{
		return std::bit_cast<native_vector_type>(_mm_cvtps_epi32(lhs));
	}
};
template<>
struct Simd_convert<3, VectorElementType::Float, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	native_vector_type operator()(native_vector_type lhs) const noexcept
	{
		return scalar_convert<2,double,int64_t>(lhs);
	}
};
