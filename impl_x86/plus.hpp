#include "../operations.hpp"
#include <functional>

template<typename T>
struct Simd_pairwise_reduce<std::plus<T>, 0, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
		//since we need to have the top 8 bits be zero we need to mask them out in the non-shifted
		//values since they would maybe be greater than zero
	        const native_vector_type low_mask = _mm_set1_epi16(0x00FF);
		const native_vector_type lhs_shifted = _mm_srli_epi16(lhs, 8);
		const native_vector_type lhs_masked = _mm_and_si128(lhs, low_mask);
		const native_vector_type lhs_added = _mm_add_epi8(lhs_masked, lhs_shifted);
		const native_vector_type rhs_shifted = _mm_srli_epi16(rhs, 8);
		const native_vector_type rhs_masked = _mm_and_si128(rhs, low_mask);
		const native_vector_type rhs_added = _mm_add_epi8(rhs_masked, rhs_shifted);
		return _mm_packus_epi16(lhs_added, rhs_added);
	}
};
template<typename T>
struct Simd_pairwise_reduce<std::plus<T>, 1, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        return _mm_hadd_epi16(lhs, rhs);
	}
};
template<typename T>
struct Simd_pairwise_reduce<std::plus<T>, 2, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        return _mm_hadd_epi32(lhs, rhs);
	}
};
template<typename T>
struct Simd_pairwise_reduce<std::plus<T>, 3, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        const native_vector_type high_pairs = _mm_movehl_ps(rhs, lhs);
	        const native_vector_type low_pairs = _mm_movelh_ps(lhs, rhs);
	        return _mm_add_epi64(high_pairs, low_pairs);
	}
};

//thankfully two's compliment addition is the same for signed/unsigned so we can just reuse the above
template<typename T>
struct Simd_pairwise_reduce<std::plus<T>, 0, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
		//since we need to have the top 8 bits be zero we need to mask them out in the non-shifted
		//values since they would maybe be greater than zero
	        const native_vector_type low_mask = _mm_set1_epi16(0x00FF);
		const native_vector_type lhs_shifted = _mm_srli_epi16(lhs, 8);
		const native_vector_type lhs_masked = _mm_and_si128(lhs, low_mask);
		const native_vector_type lhs_added = _mm_add_epi8(lhs_masked, lhs_shifted);
		const native_vector_type rhs_shifted = _mm_srli_epi16(rhs, 8);
		const native_vector_type rhs_masked = _mm_and_si128(rhs, low_mask);
		const native_vector_type rhs_added = _mm_add_epi8(rhs_masked, rhs_shifted);
		return _mm_packus_epi16(lhs_added, rhs_added);
	}
};
template<typename T>
struct Simd_pairwise_reduce<std::plus<T>, 1, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        return _mm_hadd_epi16(lhs, rhs);
	}
};
template<typename T>
struct Simd_pairwise_reduce<std::plus<T>, 2, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        return _mm_hadd_epi32(lhs, rhs);
	}
};
template<typename T>
struct Simd_pairwise_reduce<std::plus<T>, 3, VectorElementType::Signed_integer>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        const native_vector_type high_pairs = _mm_movehl_ps(rhs, lhs);
	        const native_vector_type low_pairs = _mm_movelh_ps(lhs, rhs);
	        return _mm_add_epi64(high_pairs, low_pairs);
	}
};

template<typename T>
struct Simd_pairwise_reduce<std::plus<T>, 2, VectorElementType::Float>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        return _mm_hadd_ps(lhs, rhs);
	}
};
template<typename T>
struct Simd_pairwise_reduce<std::plus<T>, 3, VectorElementType::Float>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs, native_vector_type rhs) const noexcept
	{
	        return _mm_hadd_pd(lhs, rhs);
	}
};

/*template<typename T>
struct Simd_pairwise_widen_reduce<std::plus<T>, 0, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs) const noexcept
	{
	        //same as pairwise add, except we don't pack the 16 bit values back to 8
	        const native_vector_type low_mask = _mm_set1_epi16(0x00FF);
		const native_vector_type lhs_shifted = _mm_srli_epi16(lhs, 8);
		const native_vector_type lhs_masked = _mm_and_si128(lhs, low_mask);
		return _mm_add_epi16(lhs_masked, lhs_shifted);
	}
};
template<typename T>
struct Simd_pairwise_widen_reduce<std::plus<T>, 1, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs) const noexcept
	{
	        //same as pairwise add, except we don't pack the 16 bit values back to 8
	        const native_vector_type low_mask = _mm_set1_epi32(0x0000FFFF);
		const native_vector_type lhs_shifted = _mm_srli_epi32(lhs, 16);
		const native_vector_type lhs_masked = _mm_and_si128(lhs, low_mask);
		return _mm_add_epi32(lhs_masked, lhs_shifted);
	}
};
template<typename T>
struct Simd_pairwise_widen_reduce<std::plus<T>, 2, VectorElementType::Unsigned_integer>
{
	static constexpr bool k_supported = true;
	REDUCE_BY_ALWAYS_INLINE native_vector_type operator()(native_vector_type lhs) const noexcept
	{
	        //same as pairwise add, except we don't pack the 16 bit values back to 8
		const native_vector_type lhs_shifted = _mm_srli_epi64(lhs, 32);
		constexpr int k_control_mask = 0b11001100; // C = { A[0], A[1], B[2], B[3], A[4], A[5], B[6], B[7] }
		const native_vector_type lhs_masked = _mm_blend_epi16(lhs, _mm_setzero_si128(), k_control_mask);
		return _mm_add_epi64(lhs, lhs_masked);
	}
};*/
